#define _DEFAULT_SOURCE

#include <stdio.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);


/* --- Region utilities --- */
extern inline bool region_is_invalid(const struct region *r);

/**
 * Get minimum page count to include 'mem' bytes
 */
static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

/**
 * Round up 'mem' bytes count to be divisible by OS page size
 */
static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

/**
 * Get region size matching page size constraints that could handle allocation of 'query' bytes
 */
static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}


/* --- Block utilities --- */
extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

/**
 * Get byte size of block including header and contents
 */
extern inline block_size block_full_size(const struct block_header *b);

/**
 * Check if specified block is free and capacity of the block is enough to handle allocation of query bytes
 */
static bool block_is_big_enough(struct block_header *block, size_t query) {
    return block->is_free && block->capacity.bytes >= query;
}

/**
 * Get block header pointer by block contents pointer
 */
static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - BLOCK_HEADER_SIZE);
}

/**
 * Return block that is placed in memory right after the specified one
 */
static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

/**
 * Check if second block is placed right after first block in memory
 * and they are linked
 */
static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst) && fst->next == snd;
}

/**
 * Instantiate block header at specified memory address
 * @param addr      block beginning pointer
 * @param block_sz  block full size (capacity + header)
 * @param next      next block pointer
 */
static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}


/* --- OS-level page mapping wrappers --- */
static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static void unmap_pages(void *addr, size_t length) {
    munmap(addr, length);
}


/* --- Regions and heap init/destroy --- */
/**
 * Map region at memory address specified and initialize as single block
 *
 * @param addr  region start address
 * @param query new block CAPACITY! (region size = query + block header)
 * @return      region description structure
 */
struct region alloc_region(void const *addr, size_t query) {
    query += BLOCK_HEADER_SIZE;
    query = region_actual_size(query);

    void *res = map_pages(addr, query, MAP_FIXED_NOREPLACE);
    if (res == MAP_FAILED) {
        // retry with os-selected address
        res = map_pages(addr, query, 0);
        if (res == MAP_FAILED)
            return REGION_INVALID;
    }

    block_init(res, (block_size) {query}, NULL);

    return (struct region) {
            .size = query,
            .extends = (addr == res),
            .addr = res
    };
}

/**
 * Initialize heap at HEAP_START.
 * @param initial initial size of heap (first region)
 * @return Start address of allocated region or NULL if failed to allocate
 */
void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

/**
 * Deallocate heap starting from region_start,
 * and all subsequent regions recursively by accumulating blocks in same region
 */
static void heap_term_region(void *region_start) { // NOLINT(*-no-recursion)
    if (region_start == NULL) return;
    struct block_header *cur_block = region_start;

    // Iterate through block to find region end
    size_t total_length = 0;
    while (true) {
        total_length += block_full_size(cur_block).bytes;

        if (!cur_block->next || !blocks_continuous(cur_block, cur_block->next))
            break;

        cur_block = cur_block->next;
    }
    // here cur_block points at last block in region stated at region_start
    // and cur_block->next to next region or heap end (null)

    // try unmap region starting from next block
    heap_term_region(cur_block->next);

    // do unmap for current region
    unmap_pages(region_start, total_length);
}

/**
 * Deallocate all used memory for heap starting at HEAP_START.
 * Iterate over all blocks and deallocate their regions.
 */
void heap_term() {
    heap_term_region(HEAP_START);
}


/*  --- Block splitting --- */
/**
 * Check if specified block could be split into two blocks
 * Capacity of block should be enough to create new header, create capacity for query
 * and to make sure that each of split blocks has at least BLOCK_MIN_CAPACITY
 * @param block block to split
 * @param query minimum size of second block
 * @return true if block could be split
 */
static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + BLOCK_HEADER_SIZE + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/**
 * Check if block could be split into two blocks,
 * if yes - shrink specified (FIRST) block to fit query and create new block in space left after shrink,
 * then relink blocks in order
 * @param block target block
 * @param query required minimum size of one of the blocks
 * @return if block was split
 */
static bool split_if_too_big(struct block_header *block, size_t query) {
    size_t target_capacity = size_max(query, BLOCK_MIN_CAPACITY);
    if (!block_splittable(block, target_capacity))
        return false;

    // Second block capacity should leave room for new second block header
    block_capacity next_block_capacity = (block_capacity) {block->capacity.bytes - target_capacity - BLOCK_HEADER_SIZE};

    // Update capacity of first part and find second part beginning
    block->capacity.bytes = target_capacity;
    void *next_ptr = block_after(block);

    // Instantiate block at prepared place
    block_init(next_ptr,
               size_from_capacity(next_block_capacity),
               block->next);

    block->next = next_ptr;
    return true;
}


/*  --- Block merging --- */
/**
 * Check if two block could be merged into one
 * Requires blocks be subsequent and reside in same segment
 * @param fst first block
 * @param snd second block
 * @return true if could be merged
 */
static bool blocks_mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return blocks_continuous(fst, snd) && fst->is_free && snd->is_free;
}

/**
 * Try merging two subsequent blocks starting from specified block
 *
 * @param first first merging block
 * @return      true if blocks merged successfully else function had no effect of heap
 */
static bool try_merge_with_next(struct block_header *first) {
    struct block_header *second = block_after(first);

    if (!blocks_mergeable(first, second))
        return false;

    first->next = second->next;
    first->capacity.bytes += block_full_size(second).bytes;
    return true;
}

/**
 * Merge specified block with maximum count of subsequent blocks.
 * Only joins ones could be joined to specified block. Not until heap end.
 * @param first block to start merge from
 * @return      pointer to resulting merged block (or same block if it couldn't be merged)
 */
static void try_merge_consequent(struct block_header *first) {
    if (first == NULL) return;
    while (try_merge_with_next(first));
}


/* --- Heap growth --- */
/**
 * Get last block in heap
 * @param head block to start from
 * @return     last block pointer
 */
static struct block_header *heap_last(struct block_header *head) {
    if (head == NULL) return NULL;
    while (head->next) head = head->next;
    return head;
}

/**
 * Try to allocate new region at the end of last existing region to extend it
 * Otherwise if not available, allocate at other address.
 * Connect new region first block to last existing heap block.
 * @param last  last block in existing heap
 * @param query requested size for new region
 * @return      pointer to last block after new region joined and heap optimized
 */
static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL)
        return NULL;
    // try to allocate new region at the ending of previous
    void *target_ptr = block_after(last);
    struct region next_reg = alloc_region(target_ptr, query);
    if (region_is_invalid(&next_reg))
        return NULL;

    last->next = next_reg.addr;

    // if succeeded to allocate new region at the end of previous then join blocks on the region edges
    if (next_reg.extends && try_merge_with_next(last))
        return last;
    return last->next;
}



/* --- Memory allocation --- */
/**
 * Search for first free block larger by capacity than capacity_request
 * or inform that there is no such block till the end of heap.
 * Heap is optimized by checking and merging all merge-available blocks in heap,
 * therefore this function has execution complexity of exact heap size, not till the first usable block (however it is returned).
 * @param block             block from which to start search
 * @param capacity_request  minimum required capacity in bytes
 * @return BSR_FOUND_GOOD_BLOCK if block found, BSR_REACHED_END_NOT_FOUND if no valid block found, BSR_CORRUPTED if internal structure corrupted
 */
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t capacity_request) {
    if (block == NULL) return BLOCK_SEARCH_CORRUPTED;

    struct block_search_result result = {BSR_REACHED_END_NOT_FOUND, NULL};
    do {
        try_merge_consequent(block);

        if (result.block == NULL && block_is_big_enough(block, capacity_request)) {
            result.type = BSR_FOUND_GOOD_BLOCK;
            result.block = block;
        }
    } while (block->next && (block = block->next));

    if (result.block == NULL)
        result.block = block;

    return result;
}


/**
 * Try to allocate block for request inside existing heap regions.
 * Block selection by 'find_good_or_last'.
 * If block found it could be split to form part of required size which is marked occupied and returned.
 * @param query     requested memory size in bytes
 * @param block     block to search from
 * @return BSR_FOUND_GOOD_BLOCK if required block found, BSR_REACHED_END_NOT_FOUND if no valid block found => heap extension required, BSR_CORRUPTED if internal structure corrupted
 */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result res = find_good_or_last(block, query);
    if (res.type != BSR_FOUND_GOOD_BLOCK)
        return res;

    split_if_too_big(res.block, query);
    res.block->is_free = false;
    return res;
}


/**
 * Allocate memory in heap.
 * Heap is automatically extended if not enough memory to fit request.
 * @param query         requested size in bytes
 * @param heap_start    heap first block pointer
 * @return              allocated block header pointer or NULL if failed to allocate
 */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result res = try_memalloc_existing(query, heap_start);
    if (res.type == BSR_CORRUPTED)
        return NULL;
    if (res.type == BSR_FOUND_GOOD_BLOCK)
        return res.block;

    // allocate new region for heap and retry searching
    grow_heap(heap_last(heap_start), query);

    res = try_memalloc_existing(query, heap_start);
    if (res.type == BSR_CORRUPTED)
        return NULL;
    if (res.type == BSR_FOUND_GOOD_BLOCK)
        return res.block;
    return NULL;
}


/* --- User API --- */
/**
 * Allocate in global heap block of specified size
 * @param query block size in bytes
 * @return      block contents pointer or NULL if failed to allocate block
 */
void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

/**
 * Free memory block starting at specified pointer
 * @param mem block contents pointer
 */
void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    if (header->is_free) return;
    header->is_free = true;
    try_merge_consequent(header);
}
