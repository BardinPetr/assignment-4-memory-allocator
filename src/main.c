#define PAGE_SIZE 4096

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

typedef struct block_header header;

#define DEFINE_TEST(_name) static void test_##_name()

#define RUN_SINGLE_TEST(_name) {              \
    puts("\n-> Run test \"" #_name "\"...");  \
    test_##_name();                           \
    puts("   " #_name " OK");                 \
}


static void debug_print_maps(void) {
    uint8_t maps[PAGE_SIZE * 2];

    int maps_fd = open("/proc/self/maps", O_RDONLY, 0);
    read(maps_fd, maps, PAGE_SIZE * 2 - 1);
    close(maps_fd);

    maps[PAGE_SIZE * 2 - 1] = '\0';
    printf("Maps:\n%s\n", (char *) maps);
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - BLOCK_HEADER_SIZE);
}

static void *block_get_content(void *header) {
    return header + BLOCK_HEADER_SIZE;
}

static header *_malloc_asserted(size_t sz) {
    header *ptr = block_get_header(_malloc(sz));
    assert(!ptr->is_free);
    assert(ptr->capacity.bytes == size_max(sz, BLOCK_MIN_CAPACITY));
    return ptr;
}

void _free_asserted(header *ptr) {
    _free(block_get_content(ptr));
    assert(ptr->is_free);
}

static bool in_region(void *region_start, size_t region_size, void *ptr) {
    return region_start <= ptr && ptr < (region_start + region_size);
}

// NOLINTBEGIN(*-deadcode.DeadStores)
DEFINE_TEST(malloc_normal) {
    size_t region_size = REGION_MIN_SIZE;
    void *heap = heap_init(region_size / 2);

    struct block_header *first_block = heap;

    size_t original_cap = first_block->capacity.bytes;
    assert(original_cap == (region_size - BLOCK_HEADER_SIZE));
    assert(first_block->is_free);
    assert(first_block->next == NULL);

    size_t query = 0x100;
    void *ptr = _malloc(query);

    assert(ptr == (heap + BLOCK_HEADER_SIZE));
    assert(first_block->capacity.bytes == query);
    assert(!first_block->is_free);

    assert(first_block->next == (ptr + query));
    assert(first_block->next->is_free);
    assert(first_block->next->capacity.bytes == (original_cap - query - BLOCK_HEADER_SIZE));

    heap_term();
}

DEFINE_TEST(free_one) {
    heap_init(1);

    header *h0 = _malloc_asserted(1);
    header *h1 = _malloc_asserted(1);

    assert(h0->next == h1);

    _free_asserted(h0);

    assert(h0->next == h1);
    assert(!h1->is_free);

    heap_term();
}

DEFINE_TEST(free_many) {
    heap_init(1);

    size_t cap = 0x500;
    header *h0 = _malloc_asserted(cap);
    header *h1 = _malloc_asserted(cap);
    header *h2 = _malloc_asserted(cap);
    header *h3 = _malloc_asserted(cap);

    assert(h0->next == h1);
    assert(h1->next == h2);

    _free_asserted(h2);
    _free_asserted(h2); // check double free should not change anything

    // structure should not change
    assert(h0->next == h1);
    assert(h1->next == h2);
    assert(h2->next == h3);
    assert(!h0->is_free);
    assert(!h1->is_free);
    assert(!h3->is_free);

    _free_asserted(h1);

    // check optimization (h0, h1_free, h2_free, h3) -> (h0, h1_free, h3)
    assert(h0->next == h1);
    assert(h1->next == h3);
    assert(!h0->is_free);
    assert(!h3->is_free);
    assert(h1->capacity.bytes == (2 * cap + BLOCK_HEADER_SIZE));

    heap_term();
}

DEFINE_TEST(extend_normal) {
    size_t reg0_size = REGION_MIN_SIZE;
    size_t block_size = REGION_MIN_SIZE * 2 / 3;
    void *reg0 = heap_init(1); // transforms to REGION_MIN_SIZE

    header *h0 = _malloc_asserted(block_size);
    assert(in_region(reg0, reg0_size, h0));

    header *h1 = _malloc_asserted(block_size);
    // should begin in first region without gaps and continue into second
    assert(h0->next == h1);
    assert(in_region(reg0, reg0_size, h0));
    assert((void *) (h1 + h1->capacity.bytes) > (reg0 + reg0_size));

    header *h2 = _malloc_asserted(block_size);
    assert(in_region(reg0 + reg0_size, reg0_size, h2));
    assert(h0->next == h1);
    assert(h1->next == h2);

    _free_asserted(h2);
    _free_asserted(h1);
    _free_asserted(h0);

    // should take all 2 existing regions and another one
    header *h3 = _malloc_asserted(20 * reg0_size);
    assert(reg0 == h3); // first block

    header *h_end = h3->next;
    assert(h_end->is_free);
    assert(h_end->next == NULL);

    // whole h3 should be as continuous region from heap start
    size_t real_range = (size_t) (h_end) - (size_t) (h3);
    assert(real_range == (BLOCK_HEADER_SIZE + h3->capacity.bytes));

    heap_term();
}

DEFINE_TEST(extend_far) {
    size_t region_size = REGION_MIN_SIZE;
    void *heap = heap_init(1);
    void *extend_region_start = heap + region_size;

//    debug_print_maps();
    // reserve memory at the end of existing region to prevent inplace extend
    void *tmp = mmap(extend_region_start, region_size,
                     PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
                     -1, 0
    );
    assert(tmp == extend_region_start); // without that test results are invalid

    header *h0 = _malloc_asserted(1);
    assert(h0 == heap);

    // should trigger new region allocation
    // whole block should be in second region
    size_t region2_size = region_size;
    header *h1 = _malloc_asserted(region2_size); // try to occupy whole region
    void *region2 = h1; // store new region start
    // next should be the block with left free space of 1st region
    header *h_free = h0->next;

//    debug_print_maps();
    assert(h_free->next == h1);
    assert(!in_region(heap, region_size, h1)); // not in 1st region

    // should allocate inside first region as it has free space
    header *h2 = _malloc_asserted(1);
    assert(in_region(heap, region_size, h0)); // not in 1st region

    // (reg1)->[h0|used]->[h2|used]->[anon|free]-> (reg2)->[h1|used]->...
    assert(h0->next == h2);
    assert(h2->next->next == h1);

    // free all blocks used
    _free_asserted(h0);
    _free_asserted(h1);
    _free_asserted(h2);


    // this should definitely be larger that even join of existing regions
    // therefore - new region for that block
    size_t region3_size = 10 * region_size;
    header *h3 = _malloc_asserted(region3_size);
    assert(!in_region(heap, region_size, h3));
    assert(!in_region(region2, region_size, h3));
    // should be third block - two empty blocks for old regions
    assert(((header *) heap)->next->next == h3);
    _free_asserted(h3);

    munmap(tmp, region_size);
    heap_term();
}
// NOLINTEND(*-deadcode.DeadStores)


int main() {
    RUN_SINGLE_TEST(malloc_normal);
    RUN_SINGLE_TEST(free_one);
    RUN_SINGLE_TEST(free_many);
    RUN_SINGLE_TEST(extend_normal);
    RUN_SINGLE_TEST(extend_far);
    return 0;
}
